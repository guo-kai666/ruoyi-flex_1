package com.ruoyi.common.tenant.core;

import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.orm.core.domain.BaseEntity;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 租户基类
 *
 * @author Michelle.Chung
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TenantEntity extends BaseEntity {

    /**
     * 租户编号
     */
    //@NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long tenantId;

}
