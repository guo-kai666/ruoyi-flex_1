package com.ruoyi.common.orm.core.service;

import com.mybatisflex.core.service.IService;
import com.ruoyi.common.orm.core.domain.BaseEntity;

/**
 * 自定义的服务基类接口
 *
 * @author dataprince数据小王子
 */
public interface IBaseService<T extends BaseEntity> extends IService<T> {
}
