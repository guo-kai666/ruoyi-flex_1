package com.ruoyi.common.orm.decipher;

import com.mybatisflex.core.datasource.DataSourceDecipher;
import com.mybatisflex.core.datasource.DataSourceProperty;

/**
 * 数据源解密
 *
 * @author dataprince数据小王子
 */
public class Decipher implements DataSourceDecipher {
    @Override
    public String decrypt(DataSourceProperty property, String value) {
        //解密数据源URL、用户名、密码，通过编码支持任意加密方式的解密

        String result = "";

        switch (property) {
            case URL -> result = value;
            case USERNAME -> result = value.substring(0, 4);
            case PASSWORD -> result = value.substring(0, 8);
        }
        return result;
    }
}
