package com.ruoyi.common.orm.config;

import com.github.pagehelper.PageInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Pagehelper分页，兼用老项目
 *
 * @author dataprince数据小王子
 */
@Configuration
public class PagehelperConfig {
    @Bean
    public PageInterceptor pageInterceptor(){
        PageInterceptor pageInterceptor = new PageInterceptor();
        return pageInterceptor;
    }
}
