<p align="center">
	<img alt="logo" src="https://oscimg.oschina.net/oscnet/up-d3d0a9303e11d522a06cd263f3079027715.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">Ruoyi-Flex V4.1.7</h1>
<h4 align="center">Ruoyi-Flex是基于Spring Boot V3平台 前后端分离的Java快速开发框架</h4>


## 平台简介

Ruoyi-Flex是一套全部开源的快速开发平台，使用MIT开源许可协议，毫无保留给个人及企业免费使用。基于RuoYi-Vue、RuoYi-Vue-Plus，集成MyBatis-Flex、JDK17、SpringBootV3、Lombok、Sa-Token、SpringDoc、Hutool、SpringBoot Admin、PowerJob、Vue3、Element-Plus等优秀开源软件，

* 前端采用Vue3、Element-Plus。
* 后端采用Spring Boot V3、MyBatis-Flex、Sa-Token、Redis & Jwt、PowerJob。
* 权限认证使用Jwt，支持多终端认证系统。
* 支持加载动态权限菜单，多方式轻松权限控制。
* 定制模板，使用代码生成器可以一键生成前后端代码。
* 性能卓越，功能全面，开发高效，可免除手写SQL之苦，甚至写错字段名称IDEA都会立即报警！


## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 调度中心：集成PowerJob全新一代分布式任务调度与计算框架。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：集成springdoc，根据文档注释自动生成相关的api接口文档。
15. 监控中心：集成Spring Boot Admin，监视集群系统CPU、内存、磁盘、堆栈、在线日志、Spring相关配置等。
16. 缓存监控：对系统的缓存信息查询，命令统计等。

## 开发文档

本项目提供保姆级开发文档，零基础手把手入门教程，位于/doc文件夹下面，
入门必读，请下载到本地查看:《[Ruoyi-Flex开发编译手册.docx](https://gitee.com/dataprince/ruoyi-flex/raw/master/doc/Ruoyi-Flex-Guide.docx)》。

## Ruoyi-Flex交流群

如果您看了文档，还有不明事宜，请加群交流：
<table>
    <tr>
        <td>1、普通QQ群： 100956531</td>
        <td>[Ruoyi-Flex交流一群]</td>
    </tr>
    <tr>
        <td>2、付费微信VIP交流群（需加好友捐助99元）：</td>
        <td><img src="https://gitee.com/dataprince/ruoyi-flex/raw/master/image/dataprince.jpg"/></td>
    </tr>	
</table>

## 开源协议

**为什么推荐使用本项目？**

① 本项目采用比 Apache 2.0 更宽松的 [MIT License](https://gitee.com/dataprince/ruoyi-flex/blob/master/LICENSE) 开源协议，个人与企业可 100% 免费使用，不用保留类作者、Copyright 信息。

② 代码全部开源，不会像其它项目一样，只开源部分代码，让你无法了解整个项目的架构设计。

如果这个项目让您有所收获，记得 Star 关注哦，这对我是非常不错的鼓励与支持。

## 演示图

<table>
    <tr>
        <td><img src="https://gitee.com/dataprince/ruoyi-flex/raw/master/image/login.JPG"/></td>
        <td><img src="https://gitee.com/dataprince/ruoyi-flex/raw/master/image/manul.JPG"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8074972883b5ba0622e13246738ebba237a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-9f88719cdfca9af2e58b352a20e23d43b12.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-39bf2584ec3a529b0d5a3b70d15c9b37646.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-936ec82d1f4872e1bc980927654b6007307.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-b2d62ceb95d2dd9b3fbe157bb70d26001e9.png"/></td>
        <td><img src="https://gitee.com/dataprince/ruoyi-flex/raw/master/image/springdoc.jpg"/></td>
    </tr>	 
    <tr>
        <td><img src="https://gitee.com/dataprince/ruoyi-flex/raw/master/image/powerjob.JPG"/></td>
        <td><img src="https://gitee.com/dataprince/ruoyi-flex/raw/master/image/springbootadmin.JPG"/></td>
    </tr>	
</table>

## 特别鸣谢
- [RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue)
- [RuoYi-Vue-Plus](https://gitee.com/dromara/RuoYi-Vue-Plus)
- [MyBatis-Flex](https://gitee.com/mybatis-flex/mybatis-flex)

