## 开发

```bash
# 克隆项目
git clone https://gitee.com/dataprince/ruoyi-flex

# 进入项目目录
cd ruoyi-ui

# 安装依赖
npm install --registry=https://registry.npmmirror.com

# 启动服务
npm run dev

前端浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```