-- udate to Ruoyi-Flex V4.1.1:
ALTER TABLE `gen_table_column`
	CHANGE COLUMN `table_id` `table_id` BIGINT NOT NULL COMMENT '归属表编号' AFTER `column_id`;

-- 测试菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('测试菜单', '0', '99', 'test', null, 1, 0, 'M', '0', '0', null, 'people', 'admin', sysdate(), '', null, '测试菜单');

-- 表 demo_student 结构定义
CREATE TABLE IF NOT EXISTS `demo_student` (
	`student_id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
	`student_name` varchar(30) COLLATE utf8mb4_bin DEFAULT '' COMMENT '学生名称',
	`student_age` int DEFAULT NULL COMMENT '年龄',
	`student_hobby` varchar(30) COLLATE utf8mb4_bin DEFAULT '' COMMENT '爱好（0代码 1音乐 2电影）',
	`student_sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '1' COMMENT '性别（1男 2女 3未知）',
	`student_status` char(1) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '状态（0正常 1停用）',
	`student_birthday` datetime DEFAULT NULL COMMENT '生日',
	PRIMARY KEY (`student_id`)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='学生信息单表';

-- 学生信息单表 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学生信息单表(mb)', '2018', '1', 'student', 'demo/student/index', 1, 0, 'C', '0', '0', 'demo:student:list', '#', 'admin', sysdate(), '', null, '学生信息单表(mb)菜单');
-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();
-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学生信息单表(mb)查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'demo:student:query',        '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学生信息单表(mb)新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'demo:student:add',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学生信息单表(mb)修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'demo:student:edit',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学生信息单表(mb)删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'demo:student:remove',       '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('学生信息单表(mb)导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'demo:student:export',       '#', 'admin', sysdate(), '', null, '');

-- 树表 demo_product 结构定义
CREATE TABLE IF NOT EXISTS `demo_product` (
	 `product_id` bigint NOT NULL AUTO_INCREMENT COMMENT '产品id',
	 `parent_id` bigint DEFAULT '0' COMMENT '父产品id',
	 `product_name` varchar(30) COLLATE utf8mb4_bin DEFAULT '' COMMENT '产品名称',
	`order_num` int DEFAULT '0' COMMENT '显示顺序',
	`status` char(1) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '产品状态（0正常 1停用）',
	PRIMARY KEY (`product_id`)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='产品表';

-- 产品树表（mb）菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品树表（mb）', '2018', '1', 'product', 'demo/product/index', 1, 0, 'C', '0', '0', 'demo:product:list', '#', 'admin', sysdate(), '', null, '产品树表（mb）菜单');
-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();
-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品树表（mb）查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'demo:product:query',        '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品树表（mb）新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'demo:product:add',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品树表（mb）修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'demo:product:edit',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品树表（mb）删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'demo:product:remove',       '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品树表（mb）导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'demo:product:export',       '#', 'admin', sysdate(), '', null, '');

-- 客户主表 demo_customer 结构定义
CREATE TABLE IF NOT EXISTS `demo_customer` (
	`customer_id` bigint NOT NULL AUTO_INCREMENT COMMENT '客户id',
	`customer_name` varchar(30) COLLATE utf8mb4_bin DEFAULT '' COMMENT '客户姓名',
	`phonenumber` varchar(11) COLLATE utf8mb4_bin DEFAULT '' COMMENT '手机号码',
	`sex` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '客户性别',
	`birthday` datetime DEFAULT NULL COMMENT '客户生日',
	`remark` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '客户描述',
	PRIMARY KEY (`customer_id`)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='客户主表';

-- 商品子表 demo_goods 结构定义
CREATE TABLE IF NOT EXISTS `demo_goods` (
	`goods_id` bigint NOT NULL AUTO_INCREMENT COMMENT '商品id',
	`customer_id` bigint NOT NULL COMMENT '客户id',
	`name` varchar(30) COLLATE utf8mb4_bin DEFAULT '' COMMENT '商品名称',
	`weight` int DEFAULT NULL COMMENT '商品重量',
	`price` decimal(6,2) DEFAULT NULL COMMENT '商品价格',
	`date` datetime DEFAULT NULL COMMENT '商品时间',
	`type` char(1) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品种类',
	PRIMARY KEY (`goods_id`)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='商品子表';

-- 客户主表(mb)菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户主表(mb)', '2018', '1', 'customer', 'demo/customer/index', 1, 0, 'C', '0', '0', 'demo:customer:list', '#', 'admin', sysdate(), '', null, '客户主表(mb)菜单');
-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();
-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户主表(mb)查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'demo:customer:query',        '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户主表(mb)新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'demo:customer:add',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户主表(mb)修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'demo:customer:edit',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户主表(mb)删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'demo:customer:remove',       '#', 'admin', sysdate(), '', null, '');
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户主表(mb)导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'demo:customer:export',       '#', 'admin', sysdate(), '', null, '');

-- 插入gen_table数据
INSERT INTO `gen_table` (`table_id`, `table_name`, `table_comment`, `sub_table_name`, `sub_table_fk_name`, `class_name`, `tpl_category`, `package_name`, `module_name`, `business_name`, `function_name`, `function_author`, `gen_type`, `gen_path`, `options`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES
	(1, 'demo_student', '学生信息表', NULL, NULL, 'DemoStudent', 'crud', 'com.ruoyi.demo', 'demo', 'student', '学生信息单表(mb)', '数据小王子', '0', '/', '{"parentMenuId":"2018"}', 'admin', '2023-06-03 21:44:19', '', '2023-07-09 12:14:53', '生成mybatis语法单表代码'),
	(2, 'demo_product', '产品树表', '', '', 'DemoProduct', 'tree', 'com.ruoyi.demo', 'demo', 'product', '产品树表（mb）', '数据小王子', '0', '/', '{"treeCode":"product_id","treeName":"product_name","treeParentCode":"parent_id","parentMenuId":"2018"}', 'admin', '2023-06-04 21:22:27', '', '2023-07-09 20:56:08', '生成mybatis语法树表代码'),
	(3, 'demo_customer', '客户主表', 'demo_goods', 'customer_id', 'DemoCustomer', 'sub', 'com.ruoyi.demo', 'demo', 'customer', '客户主表(mb)', '数据小王子', '0', '/', '{"parentMenuId":"2018"}', 'admin', '2023-06-04 21:43:20', '', '2023-07-11 15:49:51', '生成mybatis语法主子表代码'),
	(12, 'demo_goods', '商品子表', NULL, NULL, 'DemoGoods', 'crud', 'com.ruoyi.demo', 'demo', 'goods', '商品子', '数据小王子', '0', '/', NULL, 'admin', '2023-07-11 15:52:15', '', NULL, NULL);

-- 插入gen_table_column数据
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES
 (1, 1, 'student_id', '编号', 'int', 'Long', 'studentId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-06-03 21:44:19', '', '2023-07-09 12:14:53'),
 (2, 1, 'student_name', '学生名称', 'varchar(30)', 'String', 'studentName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-06-03 21:44:19', '', '2023-07-09 12:14:53'),
 (3, 1, 'student_age', '年龄', 'int', 'Long', 'studentAge', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-06-03 21:44:19', '', '2023-07-09 12:14:53'),
 (4, 1, 'student_hobby', '爱好（0代码 1音乐 2电影）', 'varchar(30)', 'String', 'studentHobby', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'select', 'sys_student_hobby', 4, 'admin', '2023-06-03 21:44:19', '', '2023-07-09 12:14:53'),
 (5, 1, 'student_sex', '性别（1男 2女 3未知）', 'char(1)', 'String', 'studentSex', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'select', 'sys_user_sex', 5, 'admin', '2023-06-03 21:44:19', '', '2023-07-09 12:14:53'),
 (6, 1, 'student_status', '状态（0正常 1停用）', 'char(1)', 'String', 'studentStatus', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'radio', 'sys_student_status', 6, 'admin', '2023-06-03 21:44:19', '', '2023-07-09 12:14:53'),
 (7, 1, 'student_birthday', '生日', 'datetime', 'Date', 'studentBirthday', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'datetime', '', 7, 'admin', '2023-06-03 21:44:19', '', '2023-07-09 12:14:53'),
 (8, 2, 'product_id', '产品id', 'bigint', 'Long', 'productId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-06-04 21:22:27', '', '2023-07-09 20:56:08'),
 (9, 2, 'parent_id', '父产品id', 'bigint', 'Long', 'parentId', '0', '0', NULL, '1', '1', '0', '0', 'EQ', 'input', '', 2, 'admin', '2023-06-04 21:22:27', '', '2023-07-09 20:56:08'),
 (10, 2, 'product_name', '产品名称', 'varchar(30)', 'String', 'productName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2023-06-04 21:22:27', '', '2023-07-09 20:56:08'),
 (11, 2, 'order_num', '显示顺序', 'int', 'Long', 'orderNum', '0', '0', NULL, '1', '1', '0', '0', 'EQ', 'input', '', 4, 'admin', '2023-06-04 21:22:27', '', '2023-07-09 20:56:08'),
 (12, 2, 'status', '产品状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'sys_common_status', 5, 'admin', '2023-06-04 21:22:27', '', '2023-07-09 20:56:08'),
 (13, 3, 'customer_id', '客户id', 'bigint', 'Long', 'customerId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-06-04 21:43:20', '', '2023-07-11 15:49:51'),
 (14, 3, 'customer_name', '客户姓名', 'varchar(30)', 'String', 'customerName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-06-04 21:43:20', '', '2023-07-11 15:49:51'),
 (15, 3, 'phonenumber', '手机号码', 'varchar(11)', 'String', 'phonenumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-06-04 21:43:20', '', '2023-07-11 15:49:51'),
 (16, 3, 'sex', '客户性别', 'varchar(20)', 'String', 'sex', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'sys_user_sex', 4, 'admin', '2023-06-04 21:43:20', '', '2023-07-11 15:49:51'),
 (17, 3, 'birthday', '客户生日', 'datetime', 'Date', 'birthday', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 5, 'admin', '2023-06-04 21:43:20', '', '2023-07-11 15:49:51'),
 (18, 3, 'remark', '客户描述', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 6, 'admin', '2023-06-04 21:43:20', '', '2023-07-11 15:49:51'),
 (67, 12, 'goods_id', '商品id', 'bigint', 'Long', 'goodsId', '1', '1', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-07-11 15:52:15', '', NULL),
 (68, 12, 'customer_id', '客户id', 'bigint', 'Long', 'customerId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-07-11 15:52:15', '', NULL),
 (69, 12, 'name', '商品名称', 'varchar(30)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2023-07-11 15:52:15', '', NULL),
 (70, 12, 'weight', '商品重量', 'int', 'Long', 'weight', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-07-11 15:52:15', '', NULL),
 (71, 12, 'price', '商品价格', 'decimal(6,2)', 'BigDecimal', 'price', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-07-11 15:52:15', '', NULL),
 (72, 12, 'date', '商品时间', 'datetime', 'Date', 'date', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 6, 'admin', '2023-07-11 15:52:15', '', NULL),
 (73, 12, 'type', '商品种类', 'char(1)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 7, 'admin', '2023-07-11 15:52:15', '', NULL);

-- udate to Ruoyi-Flex V4.1.2: 删除druid
delete FROM sys_menu where menu_id=111;

-- udate to Ruoyi-Flex V4.1.3: 将“系统接口”设置为外链
update sys_menu SET path="http://localhost:8080/swagger-ui/index.html", component = "", is_frame = "0", menu_type = "M" where menu_id = 117;

-- udate to Ruoyi-Flex V4.1.4: 添加sys_client系统授权表
drop table if exists sys_client;
create table sys_client (
                            id                  bigint(20)    not null            comment 'id',
                            client_id           varchar(64)   default null        comment '客户端id',
                            client_key          varchar(32)   default null        comment '客户端key',
                            client_secret       varchar(255)  default null        comment '客户端秘钥',
                            grant_type          varchar(255)  default null        comment '授权类型',
                            device_type         varchar(32)   default null        comment '设备类型',
                            active_timeout      int(11)       default 1800        comment 'token活跃超时时间',
                            timeout             int(11)       default 604800      comment 'token固定超时',
                            status              char(1)       default '0'         comment '状态（0正常 1停用）',
                            del_flag            char(1)       default '0'         comment '删除标志（0代表存在 2代表删除）',
                            create_dept         bigint(20)    default null        comment '创建部门',
                            create_by           bigint(20)    default null        comment '创建者',
                            create_time         datetime      default null        comment '创建时间',
                            update_by           bigint(20)    default null        comment '更新者',
                            update_time         datetime      default null        comment '更新时间',
                            `remark`            VARCHAR(250) NULL DEFAULT NULL COMMENT '备注' COLLATE 'utf8mb4_bin',
                            primary key (id)
) engine=innodb comment='系统授权表';

insert into sys_client values (1, 'e5cd7e4891bf95d1d19206ce24a7b32e', 'pc', 'pc123', 'password,social', 'pc', 1800, 604800, 0, 0, 103, 1, sysdate(), 1, sysdate(),'');
insert into sys_client values (2, '428a8310cd442757ae699df5d894f051', 'app', 'app123', 'password,sms,social', 'android', 1800, 604800, 0, 0, 103, 1, sysdate(), 1, sysdate(),'');

-- 删除status列
ALTER TABLE `sys_dict_data` DROP COLUMN `status`;
ALTER TABLE `sys_dict_type` DROP COLUMN `status`;

UPDATE sys_job SET create_by=1,update_by=1;

ALTER TABLE `sys_job`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `status`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

-- 表 sys_tenant 结构
CREATE TABLE IF NOT EXISTS `sys_tenant` (
    `id` bigint NOT NULL COMMENT 'id',
    `tenant_id` bigint NOT NULL COMMENT '租户编号',
    `contact_user_name` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '联系人',
    `contact_phone` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '联系电话',
    `company_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '企业名称',
    `license_number` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '统一社会信用代码',
    `address` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '地址',
    `intro` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '企业简介',
    `domain` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '域名',
    `remark` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
    `package_id` bigint DEFAULT NULL COMMENT '租户套餐编号',
    `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
    `account_count` int DEFAULT '-1' COMMENT '用户数量（-1不限制）',
    `status` char(1) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '租户状态（0正常 1停用）',
    `del_flag` char(1) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `create_dept` bigint DEFAULT NULL COMMENT '创建部门',
    `create_by` bigint DEFAULT NULL COMMENT '创建者',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_by` bigint DEFAULT NULL COMMENT '更新者',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='租户表';

-- 表sys_tenant 的数据
INSERT INTO `sys_tenant` (`id`, `tenant_id`, `contact_user_name`, `contact_phone`, `company_name`, `license_number`, `address`, `intro`, `domain`, `remark`, `package_id`, `expire_time`, `account_count`, `status`, `del_flag`, `create_dept`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES
    (1, 1, '租户管理组', '18888888888', 'XXX有限公司', NULL, NULL, 'RuoYi-Flex多租户通用后台管理管理系统', NULL, NULL, NULL, NULL, -1, '0', '0', 103, 1, '2023-08-13 08:08:08', NULL, NULL);

ALTER TABLE `sys_user`
    ADD COLUMN `tenant_id` BIGINT(19) NOT NULL DEFAULT '0' COMMENT '租户主键' AFTER `user_id`;
UPDATE sys_user SET tenant_id=1;

UPDATE sys_user SET create_by=1,update_by=1;
ALTER TABLE `sys_user`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `login_date`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

UPDATE sys_role SET create_by=1,update_by=1;
ALTER TABLE `sys_role`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `del_flag`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

UPDATE sys_post SET create_by=1,update_by=1;
ALTER TABLE `sys_post`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `status`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

UPDATE sys_notice SET create_by=1,update_by=1;
ALTER TABLE `sys_notice`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `status`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

UPDATE sys_menu SET create_by=1,update_by=1;
ALTER TABLE `sys_menu`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `icon`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

UPDATE sys_dict_type SET create_by=1,update_by=1;
ALTER TABLE `sys_dict_type`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `dict_type`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

UPDATE sys_dict_data SET create_by=1,update_by=1;
ALTER TABLE `sys_dict_data`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `is_default`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

UPDATE sys_dept SET create_by=1,update_by=1;
ALTER TABLE `sys_dept`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `del_flag`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

UPDATE sys_config SET create_by=1,update_by=1;
ALTER TABLE `sys_config`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `config_type`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

UPDATE gen_table_column SET create_by=1,update_by=1;
ALTER TABLE `gen_table_column`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `sort`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

UPDATE gen_table SET create_by=1,update_by=1;
ALTER TABLE `gen_table`
    CHANGE COLUMN `create_by` `create_by` BIGINT NOT NULL DEFAULT '0' COMMENT '创建者' AFTER `options`,
    CHANGE COLUMN `update_by` `update_by` BIGINT NOT NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;

ALTER TABLE `sys_user`
    CHANGE COLUMN `user_type` `user_type` VARCHAR(10) NULL DEFAULT '00' COMMENT '用户类型（00系统用户）' COLLATE 'utf8mb4_bin' AFTER `nick_name`;
UPDATE sys_user SET user_type="sys_user";

delete FROM sys_menu WHERE menu_name = "缓存列表";

-- “服务监控”菜单使用SpringBoot-Admin监控框架：
UPDATE `sys_menu` SET `path`='admin', `component`='monitor/admin/index', `perms`='monitor:admin:list' WHERE  `menu_id`=112;

-- update to V4.1.6：
-- ----------------------------
-- Table structure for pj_app_info
-- ----------------------------
DROP TABLE IF EXISTS `pj_app_info`;
CREATE TABLE `pj_app_info`  (
                                `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                `app_name` varchar(255) NULL DEFAULT NULL,
                                `current_server` varchar(255) NULL DEFAULT NULL,
                                `gmt_create` datetime(6) NULL DEFAULT NULL,
                                `gmt_modified` datetime(6) NULL DEFAULT NULL,
                                `password` varchar(255) NULL DEFAULT NULL,
                                PRIMARY KEY (`id`) USING BTREE,
                                UNIQUE INDEX `uidx01_app_info`(`app_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pj_app_info
-- ----------------------------
INSERT INTO `pj_app_info` VALUES (1, 'ruoyi-worker', '127.0.0.1:10010', '2023-06-13 16:32:59.263000', '2023-07-04 17:25:49.798000', '123456');

-- ----------------------------
-- Table structure for pj_container_info
-- ----------------------------
DROP TABLE IF EXISTS `pj_container_info`;
CREATE TABLE `pj_container_info`  (
                                      `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                      `app_id` bigint(0) NULL DEFAULT NULL,
                                      `container_name` varchar(255) NULL DEFAULT NULL,
                                      `gmt_create` datetime(6) NULL DEFAULT NULL,
                                      `gmt_modified` datetime(6) NULL DEFAULT NULL,
                                      `last_deploy_time` datetime(6) NULL DEFAULT NULL,
                                      `source_info` varchar(255) NULL DEFAULT NULL,
                                      `source_type` int(0) NULL DEFAULT NULL,
                                      `status` int(0) NULL DEFAULT NULL,
                                      `version` varchar(255) NULL DEFAULT NULL,
                                      PRIMARY KEY (`id`) USING BTREE,
                                      INDEX `idx01_container_info`(`app_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pj_instance_info
-- ----------------------------
DROP TABLE IF EXISTS `pj_instance_info`;
CREATE TABLE `pj_instance_info`  (
                                     `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                     `actual_trigger_time` bigint(0) NULL DEFAULT NULL,
                                     `app_id` bigint(0) NULL DEFAULT NULL,
                                     `expected_trigger_time` bigint(0) NULL DEFAULT NULL,
                                     `finished_time` bigint(0) NULL DEFAULT NULL,
                                     `gmt_create` datetime(6) NULL DEFAULT NULL,
                                     `gmt_modified` datetime(6) NULL DEFAULT NULL,
                                     `instance_id` bigint(0) NULL DEFAULT NULL,
                                     `instance_params` longtext NULL,
                                     `job_id` bigint(0) NULL DEFAULT NULL,
                                     `job_params` longtext NULL,
                                     `last_report_time` bigint(0) NULL DEFAULT NULL,
                                     `result` longtext NULL,
                                     `running_times` bigint(0) NULL DEFAULT NULL,
                                     `status` int(0) NULL DEFAULT NULL,
                                     `task_tracker_address` varchar(255) NULL DEFAULT NULL,
                                     `type` int(0) NULL DEFAULT NULL,
                                     `wf_instance_id` bigint(0) NULL DEFAULT NULL,
                                     PRIMARY KEY (`id`) USING BTREE,
                                     INDEX `idx01_instance_info`(`job_id`, `status`) USING BTREE,
                                     INDEX `idx02_instance_info`(`app_id`, `status`) USING BTREE,
                                     INDEX `idx03_instance_info`(`instance_id`, `status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pj_job_info
-- ----------------------------
DROP TABLE IF EXISTS `pj_job_info`;
CREATE TABLE `pj_job_info`  (
                                `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                `alarm_config` varchar(255) NULL DEFAULT NULL,
                                `app_id` bigint(0) NULL DEFAULT NULL,
                                `concurrency` int(0) NULL DEFAULT NULL,
                                `designated_workers` varchar(255) NULL DEFAULT NULL,
                                `dispatch_strategy` int(0) NULL DEFAULT NULL,
                                `execute_type` int(0) NULL DEFAULT NULL,
                                `extra` varchar(255) NULL DEFAULT NULL,
                                `gmt_create` datetime(6) NULL DEFAULT NULL,
                                `gmt_modified` datetime(6) NULL DEFAULT NULL,
                                `instance_retry_num` int(0) NULL DEFAULT NULL,
                                `instance_time_limit` bigint(0) NULL DEFAULT NULL,
                                `job_description` varchar(255) NULL DEFAULT NULL,
                                `job_name` varchar(255) NULL DEFAULT NULL,
                                `job_params` longtext NULL,
                                `lifecycle` varchar(255) NULL DEFAULT NULL,
                                `log_config` varchar(255) NULL DEFAULT NULL,
                                `max_instance_num` int(0) NULL DEFAULT NULL,
                                `max_worker_count` int(0) NULL DEFAULT NULL,
                                `min_cpu_cores` double NOT NULL,
                                `min_disk_space` double NOT NULL,
                                `min_memory_space` double NOT NULL,
                                `next_trigger_time` bigint(0) NULL DEFAULT NULL,
                                `notify_user_ids` varchar(255) NULL DEFAULT NULL,
                                `processor_info` varchar(255) NULL DEFAULT NULL,
                                `processor_type` int(0) NULL DEFAULT NULL,
                                `status` int(0) NULL DEFAULT NULL,
                                `tag` varchar(255) NULL DEFAULT NULL,
                                `task_retry_num` int(0) NULL DEFAULT NULL,
                                `time_expression` varchar(255) NULL DEFAULT NULL,
                                `time_expression_type` int(0) NULL DEFAULT NULL,
                                PRIMARY KEY (`id`) USING BTREE,
                                INDEX `idx01_job_info`(`app_id`, `status`, `time_expression_type`, `next_trigger_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pj_job_info
-- ----------------------------
INSERT INTO `pj_job_info` VALUES (1, '{\"alertThreshold\":0,\"silenceWindowLen\":0,\"statisticWindowLen\":0}', 1, 5, '', 2, 1, NULL, '2023-06-02 15:01:27.717000', '2023-07-04 17:22:12.374000', 1, 0, '', '单机处理器执行测试', NULL, '{}', '{\"type\":1}', 0, 0, 0, 0, 0, NULL, NULL, 'org.dromara.job.processors.StandaloneProcessorDemo', 1, 2, NULL, 1, '30000', 3);
INSERT INTO `pj_job_info` VALUES (2, '{\"alertThreshold\":0,\"silenceWindowLen\":0,\"statisticWindowLen\":0}', 1, 5, '', 1, 2, NULL, '2023-06-02 15:04:45.342000', '2023-07-04 17:22:12.816000', 0, 0, NULL, '广播处理器测试', NULL, '{}', '{\"type\":1}', 0, 0, 0, 0, 0, NULL, NULL, 'org.dromara.job.processors.BroadcastProcessorDemo', 1, 2, NULL, 1, '30000', 3);
INSERT INTO `pj_job_info` VALUES (3, '{\"alertThreshold\":0,\"silenceWindowLen\":0,\"statisticWindowLen\":0}', 1, 5, '', 1, 4, NULL, '2023-06-02 15:13:23.519000', '2023-06-02 16:03:22.421000', 0, 0, NULL, 'Map处理器测试', NULL, '{}', '{\"type\":1}', 0, 0, 0, 0, 0, NULL, NULL, 'org.dromara.job.processors.MapProcessorDemo', 1, 2, NULL, 1, '1000', 3);
INSERT INTO `pj_job_info` VALUES (4, '{\"alertThreshold\":0,\"silenceWindowLen\":0,\"statisticWindowLen\":0}', 1, 5, '', 1, 3, NULL, '2023-06-02 15:45:25.896000', '2023-06-02 16:03:23.125000', 0, 0, NULL, 'MapReduce处理器测试', NULL, '{}', '{\"type\":1}', 0, 0, 0, 0, 0, NULL, NULL, 'org.dromara.job.processors.MapReduceProcessorDemo', 1, 2, NULL, 1, '1000', 3);

-- ----------------------------
-- Table structure for pj_oms_lock
-- ----------------------------
DROP TABLE IF EXISTS `pj_oms_lock`;
CREATE TABLE `pj_oms_lock`  (
                                `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                `gmt_create` datetime(6) NULL DEFAULT NULL,
                                `gmt_modified` datetime(6) NULL DEFAULT NULL,
                                `lock_name` varchar(255) NULL DEFAULT NULL,
                                `max_lock_time` bigint(0) NULL DEFAULT NULL,
                                `ownerip` varchar(255) NULL DEFAULT NULL,
                                PRIMARY KEY (`id`) USING BTREE,
                                UNIQUE INDEX `uidx01_oms_lock`(`lock_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pj_server_info
-- ----------------------------
DROP TABLE IF EXISTS `pj_server_info`;
CREATE TABLE `pj_server_info`  (
                                   `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                   `gmt_create` datetime(6) NULL DEFAULT NULL,
                                   `gmt_modified` datetime(6) NULL DEFAULT NULL,
                                   `ip` varchar(255) NULL DEFAULT NULL,
                                   PRIMARY KEY (`id`) USING BTREE,
                                   UNIQUE INDEX `uidx01_server_info`(`ip`) USING BTREE,
                                   INDEX `idx01_server_info`(`gmt_modified`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pj_user_info
-- ----------------------------
DROP TABLE IF EXISTS `pj_user_info`;
CREATE TABLE `pj_user_info`  (
                                 `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                 `email` varchar(255) NULL DEFAULT NULL,
                                 `extra` varchar(255) NULL DEFAULT NULL,
                                 `gmt_create` datetime(6) NULL DEFAULT NULL,
                                 `gmt_modified` datetime(6) NULL DEFAULT NULL,
                                 `password` varchar(255) NULL DEFAULT NULL,
                                 `phone` varchar(255) NULL DEFAULT NULL,
                                 `username` varchar(255) NULL DEFAULT NULL,
                                 `web_hook` varchar(255) NULL DEFAULT NULL,
                                 PRIMARY KEY (`id`) USING BTREE,
                                 INDEX `uidx01_user_info`(`username`) USING BTREE,
                                 INDEX `uidx02_user_info`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pj_workflow_info
-- ----------------------------
DROP TABLE IF EXISTS `pj_workflow_info`;
CREATE TABLE `pj_workflow_info`  (
                                     `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                     `app_id` bigint(0) NULL DEFAULT NULL,
                                     `extra` varchar(255) NULL DEFAULT NULL,
                                     `gmt_create` datetime(6) NULL DEFAULT NULL,
                                     `gmt_modified` datetime(6) NULL DEFAULT NULL,
                                     `lifecycle` varchar(255) NULL DEFAULT NULL,
                                     `max_wf_instance_num` int(0) NULL DEFAULT NULL,
                                     `next_trigger_time` bigint(0) NULL DEFAULT NULL,
                                     `notify_user_ids` varchar(255) NULL DEFAULT NULL,
                                     `pedag` longtext NULL,
                                     `status` int(0) NULL DEFAULT NULL,
                                     `time_expression` varchar(255) NULL DEFAULT NULL,
                                     `time_expression_type` int(0) NULL DEFAULT NULL,
                                     `wf_description` varchar(255) NULL DEFAULT NULL,
                                     `wf_name` varchar(255) NULL DEFAULT NULL,
                                     PRIMARY KEY (`id`) USING BTREE,
                                     INDEX `idx01_workflow_info`(`app_id`, `status`, `time_expression_type`, `next_trigger_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pj_workflow_instance_info
-- ----------------------------
DROP TABLE IF EXISTS `pj_workflow_instance_info`;
CREATE TABLE `pj_workflow_instance_info`  (
                                              `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                              `actual_trigger_time` bigint(0) NULL DEFAULT NULL,
                                              `app_id` bigint(0) NULL DEFAULT NULL,
                                              `dag` longtext NULL,
                                              `expected_trigger_time` bigint(0) NULL DEFAULT NULL,
                                              `finished_time` bigint(0) NULL DEFAULT NULL,
                                              `gmt_create` datetime(6) NULL DEFAULT NULL,
                                              `gmt_modified` datetime(6) NULL DEFAULT NULL,
                                              `parent_wf_instance_id` bigint(0) NULL DEFAULT NULL,
                                              `result` longtext NULL,
                                              `status` int(0) NULL DEFAULT NULL,
                                              `wf_context` longtext NULL,
                                              `wf_init_params` longtext NULL,
                                              `wf_instance_id` bigint(0) NULL DEFAULT NULL,
                                              `workflow_id` bigint(0) NULL DEFAULT NULL,
                                              PRIMARY KEY (`id`) USING BTREE,
                                              UNIQUE INDEX `uidx01_wf_instance`(`wf_instance_id`) USING BTREE,
                                              INDEX `idx01_wf_instance`(`workflow_id`, `status`, `app_id`, `expected_trigger_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pj_workflow_node_info
-- ----------------------------
DROP TABLE IF EXISTS `pj_workflow_node_info`;
CREATE TABLE `pj_workflow_node_info`  (
                                          `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                          `app_id` bigint(0) NOT NULL,
                                          `enable` bit(1) NOT NULL,
                                          `extra` longtext NULL,
                                          `gmt_create` datetime(6) NULL,
                                          `gmt_modified` datetime(6) NULL,
                                          `job_id` bigint(0) NULL DEFAULT NULL,
                                          `node_name` varchar(255) NULL DEFAULT NULL,
                                          `node_params` longtext NULL,
                                          `skip_when_failed` bit(1) NOT NULL,
                                          `type` int(0) NULL DEFAULT NULL,
                                          `workflow_id` bigint(0) NULL DEFAULT NULL,
                                          PRIMARY KEY (`id`) USING BTREE,
                                          INDEX `idx01_workflow_node_info`(`workflow_id`, `gmt_create`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ROW_FORMAT = Dynamic;

-- “定时任务”菜单修改为“任务调度”，使用powerjob：
UPDATE `sys_menu` SET `menu_name`='任务调度',`path`='powerjob', `component`='monitor/powerjob/index', `perms`='monitor:powerjob:list' WHERE  `menu_id`=110;

-- 删除quartz数据库表：
DROP TABLE IF EXISTS QRTZ_FIRED_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_PAUSED_TRIGGER_GRPS;
DROP TABLE IF EXISTS QRTZ_SCHEDULER_STATE;
DROP TABLE IF EXISTS QRTZ_LOCKS;
DROP TABLE IF EXISTS QRTZ_SIMPLE_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_SIMPROP_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_CRON_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_BLOB_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_JOB_DETAILS;
DROP TABLE IF EXISTS QRTZ_CALENDARS;

-- V4.1.7升级到V4.1.8:
-- 修改字典"sys_user_sex"为"sys_user_gender"：
UPDATE sys_dict_data SET `dict_type`="sys_user_gender" WHERE `dict_type`="sys_user_sex";
UPDATE sys_dict_type SET `dict_type`="sys_user_gender" WHERE `dict_type`="sys_user_sex";
UPDATE gen_table_column SET `dict_type`="sys_user_gender" WHERE `dict_type`="sys_user_sex";
