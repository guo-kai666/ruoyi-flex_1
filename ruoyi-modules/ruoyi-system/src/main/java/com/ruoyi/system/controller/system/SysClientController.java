package com.ruoyi.system.controller.system;

import com.mybatisflex.core.paginate.Page;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.ruoyi.system.service.ISysClientService;
import com.ruoyi.system.domain.SysClient;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.List;

/**
 * 系统授权表 控制层。
 *
 * @author mybatis-flex-helper automatic generation
 * @since 1.0
 */
@RestController
@RequestMapping("/sysClient")
public class SysClientController {

    @Resource
    private ISysClientService sysClientService;

    /**
     * 添加 系统授权表
     *
     * @param sysClient 系统授权表
     * @return {@code true} 添加成功，{@code false} 添加失败
     */
    @PostMapping("/save")
    public boolean save(@RequestBody SysClient sysClient) {
        return sysClientService.save(sysClient);
    }


    /**
     * 根据主键删除系统授权表
     *
     * @param id 主键
     * @return {@code true} 删除成功，{@code false} 删除失败
     */
    @DeleteMapping("/remove/{id}")
    public boolean remove(@PathVariable Serializable id) {
        return sysClientService.removeById(id);
    }


    /**
     * 根据主键更新系统授权表
     *
     * @param sysClient 系统授权表
     * @return {@code true} 更新成功，{@code false} 更新失败
     */
    @PutMapping("/update")
    public boolean update(@RequestBody SysClient sysClient) {
        return sysClientService.updateById(sysClient);
    }


    /**
     * 查询所有系统授权表
     *
     * @return 所有数据
     */
    @GetMapping("/list")
    public List<SysClient> list() {
        return sysClientService.list();
    }


    /**
     * 根据系统授权表主键获取详细信息。
     *
     * @param id sysClient主键
     * @return 系统授权表详情
     */
    @GetMapping("/getInfo/{id}")
    public SysClient getInfo(@PathVariable Serializable id) {
        return sysClientService.getById(id);
    }


    /**
     * 分页查询系统授权表
     *
     * @param page 分页对象
     * @return 分页对象
     */
    @GetMapping("/page")
    public Page<SysClient> page(Page<SysClient> page) {
        return sysClientService.page(page);
    }
}
