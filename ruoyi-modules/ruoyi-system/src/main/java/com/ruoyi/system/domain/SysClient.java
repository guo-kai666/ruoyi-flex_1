package com.ruoyi.system.domain;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import com.ruoyi.common.orm.core.domain.BaseEntity;
import com.ruoyi.common.orm.listener.EntityInsertListener;
import com.ruoyi.common.orm.listener.EntityUpdateListener;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 授权管理对象 sys_client
 *
 * @author dataprince数据小王子
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "sys_client")
public class SysClient extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @Id()
    private Long id;

    /**
     * 客户端id
     */
    private String clientId;

    /**
     * 客户端key
     */
    private String clientKey;

    /**
     * 客户端秘钥
     */
    private String clientSecret;

    /**
     * 授权类型
     */
    private String grantType;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * token活跃超时时间
     */
    private Long activeTimeout;

    /**
     * token固定超时时间
     */
    private Long timeout;

    /**
     * 状态（0正常 1停用）
     */
    private String status;

    /**
     * 删除标志（0代表存在 1代表删除）
     */
    @Column(isLogicDelete = true)
    private String delFlag;


}
