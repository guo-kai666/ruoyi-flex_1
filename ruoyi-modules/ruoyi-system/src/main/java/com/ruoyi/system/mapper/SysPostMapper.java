package com.ruoyi.system.mapper;

import java.util.List;

import com.mybatisflex.core.BaseMapper;
import com.ruoyi.system.domain.SysPost;
import org.apache.ibatis.annotations.Mapper;

/**
 * 岗位信息 数据层
 *
 * @author 数据小王子
 */
@Mapper
public interface SysPostMapper extends BaseMapper<SysPost>
{
    /**
     * 根据用户ID获取岗位选择框列表
     *
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    List<Long> selectPostListByUserId(Long userId);

    /**
     * 查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    List<SysPost> selectPostsByUserName(String userName);

}
