package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.ruoyi.common.core.core.page.PageDomain;
import com.ruoyi.common.core.core.page.TableSupport;
import com.ruoyi.common.core.utils.MapstructUtils;
import com.ruoyi.common.core.utils.sql.SqlUtil;
import com.ruoyi.common.orm.core.page.TableDataInfo;
import com.ruoyi.common.orm.core.service.impl.BaseServiceImpl;
import com.ruoyi.system.domain.bo.SysPostBo;
import com.ruoyi.system.domain.vo.SysPostVo;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.system.domain.SysPost;
import com.ruoyi.system.mapper.SysPostMapper;
import com.ruoyi.system.mapper.SysUserPostMapper;
import com.ruoyi.system.service.ISysPostService;

import static com.ruoyi.system.domain.table.SysPostTableDef.SYS_POST;

/**
 * 岗位信息 服务层处理
 *
 * @author ruoyi
 */
@Service
public class SysPostServiceImpl extends BaseServiceImpl<SysPostMapper, SysPost> implements ISysPostService
{
    @Resource
    private SysPostMapper postMapper;

    @Resource
    private SysUserPostMapper userPostMapper;

    @Override
    public QueryWrapper query() {
        return super.query().from(SYS_POST);
    }

    /**
     * 根据postBo构建QueryWrapper查询条件
     * @param postBo
     * @return 查询条件
     */
    private QueryWrapper buildQueryWrapper(SysPostBo postBo) {
        QueryWrapper queryWrapper = super.buildBaseQueryWrapper();
        PageDomain pageDomain = TableSupport.buildPageRequest();

        if (StringUtils.isNotEmpty(postBo.getPostCode())) {
            queryWrapper.and(SYS_POST.POST_CODE.like(postBo.getPostCode()));
        }
        if (StringUtils.isNotEmpty(postBo.getStatus())) {
            queryWrapper.and(SYS_POST.STATUS.eq(postBo.getStatus()));
        }
        if (StringUtils.isNotEmpty(postBo.getPostName())) {
            queryWrapper.and(SYS_POST.POST_NAME.like(postBo.getPostName()));
        }

        if (StringUtils.isNotEmpty(pageDomain.getOrderBy())) {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            queryWrapper.orderBy(orderBy);
        }
        return queryWrapper;
    }

    /**
     * 查询岗位信息集合
     *
     * @param postBo 岗位信息
     * @return 岗位信息集合
     */
    @Override
    public List<SysPostVo> selectPostList(SysPostBo postBo)
    {
        QueryWrapper queryWrapper = buildQueryWrapper(postBo);
        return this.listAs(queryWrapper, SysPostVo.class);
    }

    /**
     * 分页查询公告列表
     *
     * @param postBo 公告信息
     * @return 公告集合
     */
    @Override
    public TableDataInfo<SysPostVo> selectPage(SysPostBo postBo) {
        QueryWrapper queryWrapper = buildQueryWrapper(postBo);

        PageDomain pageDomain = TableSupport.buildPageRequest();

        Page<SysPostVo> page = this.getMapper().paginateAs(pageDomain.getPageNum(), pageDomain.getPageSize(), queryWrapper, SysPostVo.class);
        return TableDataInfo.build(page);
    }

    /**
     * 查询所有岗位
     *
     * @return 岗位列表
     */
    @Override
    public List<SysPostVo> selectPostAll()
    {
        return this.listAs(query(), SysPostVo.class);
    }

    /**
     * 通过岗位ID查询岗位信息
     *
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    @Override
    public SysPostVo selectPostById(Long postId)
    {
        return this.getOneAs(query().where(SYS_POST.POST_ID.eq(postId)), SysPostVo.class);
    }

    /**
     * 根据用户ID获取岗位选择框列表
     *
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    @Override
    public List<Long> selectPostListByUserId(Long userId)
    {
        return postMapper.selectPostListByUserId(userId);
    }

    /**
     * 校验岗位名称是否唯一
     *
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public boolean checkPostNameUnique(SysPostBo post)
    {
        Long postId = StringUtils.isNull(post.getPostId()) ? -1L : post.getPostId();
        SysPost info = this.getOne(query().where(SYS_POST.POST_NAME.eq(post.getPostName())));
        if (StringUtils.isNotNull(info) && info.getPostId().longValue() != postId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验岗位编码是否唯一
     *
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public boolean checkPostCodeUnique(SysPostBo post)
    {
        Long postId = StringUtils.isNull(post.getPostId()) ? -1L : post.getPostId();
        SysPost info = this.getOne(query().where(SYS_POST.POST_CODE.eq(post.getPostCode())));
        if (StringUtils.isNotNull(info) && info.getPostId().longValue() != postId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param postId 岗位ID
     * @return 结果
     */
    @Override
    public int countUserPostById(Long postId)
    {
        return userPostMapper.countUserPostById(postId);
    }

    /**
     * 删除岗位信息
     *
     * @param postId 岗位ID
     * @return 结果:true 删除成功，false 删除失败。
     */
    @Override
    public boolean deletePostById(Long postId)
    {
        return this.removeById(postId);
    }

    /**
     * 批量删除岗位信息
     *
     * @param postIds 需要删除的岗位ID
     * @return 结果:true 删除成功，false 删除失败。
     */
    @Override
    public boolean deletePostByIds(Long[] postIds)
    {
        for (Long postId : postIds)
        {
            SysPostVo post = selectPostById(postId);
            if (countUserPostById(postId) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配，不能删除！", post.getPostName()));
            }
        }
        return this.removeByIds(Arrays.asList(postIds));
    }

    /**
     * 新增保存岗位信息
     *
     * @param postBo 岗位信息
     * @return 结果
     */
    @Override
    public int insertPost(SysPostBo postBo)
    {
        SysPost sysPost = MapstructUtils.convert(postBo, SysPost.class);
        return postMapper.insert(sysPost,false);
    }

    /**
     * 修改保存岗位信息
     *
     * @param postBo 岗位信息
     * @return 结果:true 更新成功，false 更新失败
     */
    @Override
    public boolean updatePost(SysPostBo postBo)
    {
        SysPost sysPost = MapstructUtils.convert(postBo, SysPost.class);
        return this.updateById(sysPost);
    }
}
